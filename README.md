Your swiss army knife for dealing with .gitignore.

1) Build the respective main.go
2) Move the resulting binary to $(git --exec-path)
3) In the shell switch to your project directory and do `git rekt <your_file>`

The file or directory will be removed, the deletion will be committed and your file 
will appear in the project root's .gitignore - file.