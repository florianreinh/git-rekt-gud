package main

import (
	"os"
	"path/filepath"
	"fmt"
	"strings"
	"os/exec"
	"git-rekt-gud/src/common/log"
	specifics "git-rekt-gud/src/common/platform_specifics"
)

const gitIgnoreFilename = ".gitignore"
var gitRootDir string

/* Possible CLI options: --cached (see git help rm), -f (custom target .gitignore file as path)
   Extension app signature: git rekt <FILE_TO_RM_AND_IGNORE> */
func main() {
	targetFile := os.Args[1]
	log.SetDefaultLogLevel("info")

	setRootDir()
	targetFileGitIgnoreEntry := rootGitIgnoreRelativeFilePath(targetFile)

	doGitRm(targetFile)
	addToGitignore(targetFileGitIgnoreEntry)
}

func rootGitIgnoreRelativeFilePath(file string) string {
	absoluteFilePath, absError := filepath.Abs(file)
	log.FatalIfExists(fmt.Sprintf("Could not get absolute file path for %s:", file), absError)

	log.Instance().Debug("target file: " + absoluteFilePath)
	log.Instance().Debug("git base: " + gitRootDir)

	return strings.ReplaceAll(absoluteFilePath, gitRootDir, "")
}

func setRootDir() {
	out, getTopLevelErr := exec.Command("git", "rev-parse", "--show-toplevel").Output()
	log.FatalIfExists("git rm failed.", getTopLevelErr)

	gitRootDir = strings.TrimSpace(string(out))
	log.Instance().Info("git root dir: " + gitRootDir)
}

func doGitRm(file string) {
	// perform git rm file
	cmd := exec.Command("git", "rm", "-r", file)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	rmError := cmd.Run()

	log.FatalIfExists("git rm failed.", rmError)

	// commit the delete change
	commit := exec.Command("git", "commit", "-a", "-m", fmt.Sprintf("Remove %s", file))
	commit.Stdout = os.Stdout
	commit.Stderr = os.Stderr

	commitError := commit.Run()
	log.FatalIfExists("git commit failed.", commitError)

	log.Instance().Info(fmt.Sprintf("git rm %s DONE", file))
}

func addToGitignore(fileStr string) {
	file, err := os.OpenFile(rootGitIgnore(), os.O_APPEND | os.O_CREATE | os.O_WRONLY, 0644)
	log.FatalIfExists("Could not open .gitignore.", err)

	defer file.Close()

	_, writeErr := file.WriteString(fileStr + specifics.LineBreak)
	log.FatalIfExists("There was an error writing .gitignore.", writeErr)
}

func rootGitIgnore() string{
	return fmt.Sprintf("%s/%s", gitRootDir, gitIgnoreFilename)
}
