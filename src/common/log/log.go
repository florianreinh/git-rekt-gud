package log

import "github.com/sirupsen/logrus"

func FatalIfExists(msg string, err error){
	if err != nil {logrus.Fatal(msg, err)}
}

func Instance() *logrus.Logger{
	return logrus.StandardLogger()
}

func SetDefaultLogLevel(levelStr string) {
	level,parseErr := logrus.ParseLevel(levelStr)
	FatalIfExists("Could not set log level: " + levelStr, parseErr)

	logrus.StandardLogger().SetLevel(level)
}




